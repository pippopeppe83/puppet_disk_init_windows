# disk_init_win

#### Table of Contents

1. [Description](#description)
1. [Setup - The basics of getting started with disk_init_win](#setup)
    * [Beginning with disk_init_win](#beginning-with-disk_init_win)
1. [Limitations - OS compatibility, etc.](#limitations)
1. [Development - Guide for contributing to the module](#development)

## Description

This module initializes all the new disks added to a Windows machine.
Using powershell the module first finds disks that are not initialized yet, it secondly formats these disks and assigns the first available drive letter.
It was tested with Windows 2012 R2 , it does not work with Windows 2008 because it requires powershell 3.0.

## Setup


### Beginning with disk_init_win

It is enough to add in your nodes 

  include disk_init_win

## Limitations

I tested it with many machines but always with Windows 2012 R2 and puppet 3.7 , it can probably work also on newer versions. 

## Development

For any feedback, corrections, or improvements write me at giuseppeborgese@gmail.com

## Release Notes/Contributors/Etc. 

In the module I used this line to extract the first drive letter available 
for($j=67;gdr($d=[char]++$j)2>0){}$d
I found this line in this page http://www.powershellmagazine.com/2012/01/12/find-an-unused-drive-letter/
