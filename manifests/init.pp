# Class: disk_init_win
# ===========================
#
# This class using powershell inizialize and format every new disk not yet inizialized in 
# a windows machine. It is tested with Windows 2012 R2 and it is not working with Windows 2008
#
# Parameters
# ----------
#
# No parameters necessary.
#
# Variables
# ----------
#
#  No variables necessary
#
# Examples
# --------
#
# @example
#    include disk_init_win
#
# Authors
# -------
#
# Giuseppe Borgese giuseppeborgese@gmail.com
#
# Copyright
# ---------
#
# Copyright 2016 Giuseppe Borgese.
#


class disk_init_win{
  notify { "Calling function initialize all the disks": }
  if ! ($::operatingsystemrelease == '2008 R2') {
    exec { 'inizialize all the disks':
      command  => '$num=(Get-Disk | Where-Object {$_. OperationalStatus -eq \'Offline\'}) | select Number ; $disknum = $null ; $num | ForEach-Object { $disknum = ($_).Number; $driveletter = $(for($j=67;gdr($d=[char]++$j)2>0){}$d) ; Initialize-Disk -Number $disknum -PartitionStyle GPT ; New-Partition -DiskNumber $disknum -AssignDriveLetter -UseMaximumSize ; Format-Volume -DriveLetter $driveletter -FileSystem NTFS -Confirm:$false }',
      onlyif   => 'if((Get-Disk | Where-Object {$_.OperationalStatus -eq \'Offline\'}) -ne $null){}else{exit 1}',
      timeout  => '100',
      provider => powershell,
    }
  }
  
  $major_release = $::facts['os']['release']['major']
  notify { "Executed initialize all the disks for Windows ${major_release}": }
}
